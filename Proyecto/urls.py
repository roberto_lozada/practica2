from django.contrib import admin
from django.urls import path, include
from login.views import SignupView
from login import urls
from keys.views import KeyViewSet
from login.views import UserViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'keys', KeyViewSet)
router.register(r'user', UserViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(urls)),
    path('viewsets/', include(router.urls))

]
