from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.QtWidgets import *
import sys
import requests

emailGlobal = ""
datos = []

def iniciar_registro():
    ventana_registro.show()


def guardar_registro():
    username = ventana_registro.txt_usuario.text()
    correo = ventana_registro.txt_correo.text()
    password = ventana_registro.txt_contra.text()
    url = 'http://127.0.0.1:8000/api/auth/signup/'
    usuario = {'email': str(correo), 'username': str(
        username), 'password': str(password)}
    x: requests = requests.post(url, data=usuario)
    if(x.ok):
        print("Registro completo")
        ventana_registro.close()
    else:
        dlg = QMessageBox()
        dlg.setWindowTitle("ERROR")
        dlg.setText("Correo incorrecto")
        button = dlg.exec()
    ventana_registro.txt_usuario.setText("")
    ventana_registro.txt_correo.setText("")
    ventana_registro.txt_contra.setText("")

def getDatos():
    url = "http://localhost:8000/api/auth/getdata/?email="+emailGlobal
    request = requests.get(url)
    print(request.json())
    datos = request.json()
    texto = ""
    for dato in datos:
        print(dato['email'])
        print(dato['password'])
        print(dato['descripcion'])
        texto += dato['email'] + '\n' + dato['password'] + '\n' + dato['descripcion'] +'\n'

    
    ventana_info.txt_datos.setText(texto)
    




def iniciar_sesion():
    email = ventana_inicio.txt_usuario.text()
    global emailGlobal
    emailGlobal = str(email)
    password = ventana_inicio.txt_contra.text()
    usuario = {'email': email, 'password': password}
    x = requests.post('http://127.0.0.1:8000/api/auth/login/', data=usuario)
    if(x.ok):
        ventana_inicio.txt_usuario.setText("")
        ventana_inicio.txt_contra.setText("")
        ventana_info.show()
        ventana_info.email.setText(emailGlobal)

        ventana_info.email.setAlignment(QtCore.Qt.AlignCenter)
        ventana_inicio.close()
        getDatos()

    else:
        ventana_inicio.txt_usuario.setText("")
        ventana_inicio.txt_contra.setText("")
        dlg = QMessageBox()
        dlg.setWindowTitle("ERROR")
        dlg.setText("Correo o contraseña incorrectas")
        button = dlg.exec()


def desconectar():
    ventana_info.close()
    ventana_info.email.setText("")
    ventana_inicio.show()
    #print(emailGlobal)


def iniciar_guardarInfo():
    ventana_guardarInfo.show()

def guardar_info():
    ventana_guardarInfo.close()
    ventana_guardarInfo.txt_email.setText("")
    ventana_guardarInfo.txt_password.setText("")
    ventana_guardarInfo.txt_descrip.setPlainText("")
app = QtWidgets.QApplication([])

ventana_inicio = uic.loadUi('ventanas/inicio.ui')
ventana_registro = uic.loadUi('ventanas/registro.ui')
ventana_info = uic.loadUi('ventanas/info.ui')
ventana_guardarInfo = uic.loadUi('ventanas/registroInfo.ui')
ventana_inicio.show()
ventana_inicio.btn_registrarse.clicked.connect(iniciar_registro)
ventana_registro.btn_guardar.clicked.connect(guardar_registro)
ventana_inicio.btn_iniciar.clicked.connect(iniciar_sesion)
ventana_registro.txt_contra.setEchoMode(QLineEdit.Password)
ventana_inicio.txt_contra.setEchoMode(QLineEdit.Password)
ventana_info.btn_desconectar.clicked.connect(desconectar)
ventana_info.btn_agregar.clicked.connect(iniciar_guardarInfo)
ventana_guardarInfo.btn_guardar.clicked.connect(guardar_info)
ventana_info.tbl_info.setHorizontalHeaderLabels(['Correo', 'Contraseña', 'Descripcion'])
ventana_info.tbl_info.setSelectionBehavior(QAbstractItemView.SelectRows)
ventana_info.tbl_info.setEditTriggers(QAbstractItemView.NoEditTriggers)

sys.exit(app.exec())
