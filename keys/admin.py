from django.contrib import admin
from .models import Key


class KeyAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'email', 'password', 'descripcion')


admin.site.register(Key, KeyAdmin)
