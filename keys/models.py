from django.db import models
from django.conf import settings


class Key(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.PROTECT)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    descripcion = models.TextField()

    def __str__(self) -> str:
        return self.user.email
