from rest_framework import serializers
from .models import Key
from login.serializers import UserSerializer


class KeySerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Key.objects.all())

    class Meta:
        model = Key
        fields = ['id', 'user', 'user_id', 'email', 'password', 'descripcion']
