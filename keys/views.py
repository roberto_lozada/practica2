from functools import partial
from rest_framework.response import Response
from .models import Key
from .serializers import KeySerializer
from rest_framework import viewsets

class KeyViewSet(viewsets.ModelViewSet):
    queryset = Key.objects.all()
    serializer_class = KeySerializer

    def create(self, request, *args, **kwargs):
        #import pdb; pdb.set_trace()
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(
                user_id = request.data['user_id']
            )
            return Response(serializer.data)
        else:
            return Response("Datos no validos")
    
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, partial=partial)
        #import pdb; pdb.set_trace()
        if serializer.is_valid():
            serializer.save(
                user_id = request.data['user_id']
            )
            return Response(serializer.data)
        else:
            return Response("Datos no validos")