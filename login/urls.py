from django.urls import path, include
from .views import SignupView, LoginView, LogoutView, UserViewSet, getData


urlpatterns = [
    path('auth/signup/', SignupView.as_view(), name="auth_singnup"),
    path('auth/login/', LoginView.as_view(), name='auth_login'),
    path('auth/logout/', LogoutView.as_view(), name='auth_logout'),
    path('auth/getuser/', UserViewSet.as_view({'get' : 'list'}), name="getuser"),
    path('auth/getdata/', getData, name="getdata")

]