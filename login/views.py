from rest_framework import generics, status
from .serializers import UserSerializer
from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets
from .models import CustomUser
from rest_framework.decorators import action, api_view
from django.core import serializers as serializerDjango
from keys.models import Key

@api_view()
def getData(request):
    email = request.query_params['email']
    try:
        query = Key.objects.filter(user__email = email)
        #data = serializerDjango.serialize("json", query.values())
        return Response(data=query.values(), status=status.HTTP_200_OK)
    except Exception as err:
        return Response(data={"Message" : "Error en la consulta", "Error" : str(err)}, status=status.HTTP_404_NOT_FOUND)




class UserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

    @action(detail=False, methods=['get'])
    def getbyemail(self, request):
        email = request.query_params['email']
        return Response(data={"Message": email}, status=status.HTTP_200_OK)


class LoginView(APIView):
    def post(self, request):
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        user = authenticate(email=email, password=password)

        if user:
            login(request, user)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(data={"Message": "Credenciales incorrectas"},
                            status=status.HTTP_404_NOT_ACCEPTABLE)


class LogoutView(APIView):
    def post(self, request):
        logout(request)

        return Response(status=status.HTTP_200_OK)


class SignupView(generics.CreateAPIView):
    serializer_class = UserSerializer
